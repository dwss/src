# For UART communication on Raspberry Pi using Pyhton edited and
# modified code from http://www.electronicwings.com

#Function to send email at end of day
def send_data():
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	from email.MIMEBase import MIMEBase
	from email import encoders
	fromaddr = "dwssteam@gmail.com"
	toaddr = "studentar99@gmail.com"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "Daily Readings"
	body = "This is a summary of the data recorded for today. Attached"\
	" below is the excel sheet."
	msg.attach(MIMEText(body, 'plain'))
	filename = "data.csv"
	attachment = open("/home/pi/data.csv", "rb")
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" 
		% filename)
	msg.attach(part)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "y36Jy$EMnu!Fw4")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
	
#Function to send email to open window
def open_window():
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	from email.MIMEBase import MIMEBase
	from email import encoders
	fromaddr = "dwssteam@gmail.com"
	toaddr = "studentar99@gmail.com"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "It is time to open a Window"
	body = "Based off of the data recorded so far it would be best for" \
	" you to open a window to heat up the environment. Below is also "\
	"the data recorded so far."
	msg.attach(MIMEText(body, 'plain'))
	filename = "data.csv"
	attachment = open("/home/pi/data.csv", "rb")
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" 
		% filename)
	msg.attach(part)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "y36Jy$EMnu!Fw4")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
	
#Function to send email to close window
def close_window():
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	from email.MIMEBase import MIMEBase
	from email import encoders
	fromaddr = "dwssteam@gmail.com"
	toaddr = "studentar99@gmail.com"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "It is time to close a window"
	body = "Based off of the data recorded so far it would be best" \
	" for you to close a window to cool down the environment." \
	" Below is also the data recorded so far."
	msg.attach(MIMEText(body, 'plain'))
	filename = "data.csv"
	attachment = open("/home/pi/data.csv", "rb")
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" 
		% filename)
	msg.attach(part)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "y36Jy$EMnu!Fw4")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
		
#Function to send email that vent is on
def fan_on():
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	from email.MIMEBase import MIMEBase
	from email import encoders
	fromaddr = "dwssteam@gmail.com"
	toaddr = "studentar99@gmail.com"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "The vent has been turned on"
	body = "Based off of the data recorded so far the vent was turned" \
	" on because it was too hot. This is an attempt to cool down" \
	" the environment."
	msg.attach(MIMEText(body, 'plain'))
	filename = "data.csv"
	attachment = open("/home/pi/data.csv", "rb")
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" 
		% filename)
	msg.attach(part)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "y36Jy$EMnu!Fw4")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
		
#Function to send email that fan is off
def fan_off():
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	from email.MIMEBase import MIMEBase
	from email import encoders
	fromaddr = "dwssteam@gmail.com"
	toaddr = "studentar99@gmail.com"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "The vent has been turned off"
	body = "Based off of the data recorded so far the vent was turned" \
	" off because it was cool enough. This is an attempt to keep" \
	" the cool environment."
	msg.attach(MIMEText(body, 'plain'))
	filename = "data.csv"
	attachment = open("/home/pi/data.csv", "rb")
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" 
		% filename)
	msg.attach(part)
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "y36Jy$EMnu!Fw4")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
	
#Preset states to not repeat emails sent to user
fan_state = 2
window_state = 2

import csv

#Create/Wipe csv file named data.csv
file = open("/home/pi/data.csv","w")	#data.csv can be 
										#[insert file name here].csv
		#Use csv.writer to write to the csv file and create colums for
		#date, time, and data
writer = csv.writer(file)
writer.writerow(["Date", "Time", "Temperature", 
"Light Level","Vent Status"])

while True:
	import serial
	import time
	from datetime import date
	from datetime import datetime
	from time import sleep
	import csv
	
	#Obtain the date
	today = date.today()
	
	#Open port to start using UART
	ser = serial.Serial ("/dev/ttyUSB0", 38400)    #Open port with baud 
													#rate
	
	time_now1 = datetime.now()					#Obtain the time then
												#display in nice format
	current_time1 = time_now1.strftime("%I:%M:%S %p")	
	# print(current_time1)	#debugging
	
	#Change the time to when you want a summary email
	time_test = "03:53:00 PM"
	time_test2 = "03:53:01 PM"
	
	if time_test  not in current_time1:
	    received_data = ser.read()              #read serial port
	    sleep(0.03)
	    data_left = ser.inWaiting()             #check for remaining 
												#byte
	    received_data += ser.read(data_left)
	    # print (received_data)                   #print received data
	    data_split = received_data.split('\r\n')
	    vent = data_split[2]
	    temp = data_split[1]
	    light = data_split[0]
	    print(temp)
	    print(light)
	    print(vent)
	    import re
	    data_num = [float(s) for s in re.findall(r'-?\d+\.?\d*', 
	    received_data)]
	    # print(data_num)	#debugging
	    temp_num = data_num[1]
	    light_num = data_num[0]

	    ser.write(received_data)                #transmit data serially 
	    
	    date = today.strftime("%b-%d-%Y")		#grab and display date
	    
	    time_now = datetime.now()					#Obtain the time then
												#display in nice format
	    current_time = time_now.strftime("%I:%M:%S %p")	
	    
	    #Write the date, time, and data that you recieved through UART
	    file = open("/home/pi/data.csv","a")	
	    writer = csv.writer(file)
	    writer.writerow([date, current_time, temp, light,vent])
	    file.close()

	    # print(int(temp_num))		#these print statements are for 
									#debugging
	    # print(int(light_num))
	    
	    #Following 4 if statements setup the test cases
	    if int(temp_num) > 28 and int(light_num) >= 3 and window_state ==2:
			close_window()
			window_state = 1
	    if int(temp_num) <= 28 and int(light_num) < 3 and window_state ==1:
			open_window()
			window_state = 2
	    if "The fan is on" in vent and fan_state == 2:
			fan_on()
			fan_state = 1
	    if "The fan is off" in vent and fan_state == 1:
			fan_off()
			fan_state = 2
			
	#These last 2 if statements are to make sure the summary email sends
	if time_test in current_time1:
		#Sending email of data.csv
		send_data()
		
		#Create/Wipe csv file named data.csv
		file = open("/home/pi/data.csv","w")	#data.csv can be 
											#[insert file name here].csv
		#Use csv.writer to write to the csv file and create colums for
		#date, time, and data
		writer = csv.writer(file)
		writer.writerow(["Date", "Time", "Temperature", 
		"Light Level","Vent Status"])
		
	if time_test2 in current_time1:
		#Sending email of data.csv
		send_data()
		
		#Create/Wipe csv file named data.csv
		file = open("/home/pi/data.csv","w")	#data.csv can be 
											#[insert file name here].csv
		#Use csv.writer to write to the csv file and create colums for
		#date, time, and data
		writer = csv.writer(file)
		writer.writerow(["Date", "Time", "Temperature", 
		"Light Level","Vent Status"])
